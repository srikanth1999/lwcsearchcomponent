import { LightningElement, api, track, wire } from "lwc";

export default class Search extends NavigationMixin(LightningElement) {
	
	@track findedInstitutionSales = [];
	@track searchInstitutionSales = [];
	@track currentOrdersList =["test","test1","org","xyz"];
	
	handleSearchInstSales(event){
    this.findedInstitutionSales = [];
    this.searchInstitutionSales = event.target.value;
    console.log("searchInstitutionSales " + this.searchInstitutionSales);
    for (var i = 0; i < this.tempInstitutionSales.length; i++) {
      if (
        JSON.stringify(this.tempInstitutionSales[i])
          .toLowerCase()
          .includes(this.searchInstitutionSales.toLowerCase())
      ) {
        this.findedInstitutionSales = [...this.findedInstitutionSales, this.tempInstitutionSales[i]];
      }
    }
    this.currentOrdersList = this.findedInstitutionSales;
    
  }

}